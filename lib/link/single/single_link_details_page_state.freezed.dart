// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named

part of 'single_link_details_page_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

class _$SingleLinkDetailsPageStateTearOff {
  const _$SingleLinkDetailsPageStateTearOff();

  SingleLinkDetailsPageLoadingState loading({@required Link link}) {
    return SingleLinkDetailsPageLoadingState(
      link: link,
    );
  }

  SingleLinkDetailsPageSuccessState success(
      {@required Link link, @required List<Comment> comments}) {
    return SingleLinkDetailsPageSuccessState(
      link: link,
      comments: comments,
    );
  }
}

// ignore: unused_element
const $SingleLinkDetailsPageState = _$SingleLinkDetailsPageStateTearOff();

mixin _$SingleLinkDetailsPageState {
  Link get link;

  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result loading(Link link),
    @required Result success(Link link, List<Comment> comments),
  });
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result loading(Link link),
    Result success(Link link, List<Comment> comments),
    @required Result orElse(),
  });
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result loading(SingleLinkDetailsPageLoadingState value),
    @required Result success(SingleLinkDetailsPageSuccessState value),
  });
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result loading(SingleLinkDetailsPageLoadingState value),
    Result success(SingleLinkDetailsPageSuccessState value),
    @required Result orElse(),
  });

  $SingleLinkDetailsPageStateCopyWith<SingleLinkDetailsPageState> get copyWith;
}

abstract class $SingleLinkDetailsPageStateCopyWith<$Res> {
  factory $SingleLinkDetailsPageStateCopyWith(SingleLinkDetailsPageState value,
          $Res Function(SingleLinkDetailsPageState) then) =
      _$SingleLinkDetailsPageStateCopyWithImpl<$Res>;
  $Res call({Link link});
}

class _$SingleLinkDetailsPageStateCopyWithImpl<$Res>
    implements $SingleLinkDetailsPageStateCopyWith<$Res> {
  _$SingleLinkDetailsPageStateCopyWithImpl(this._value, this._then);

  final SingleLinkDetailsPageState _value;
  // ignore: unused_field
  final $Res Function(SingleLinkDetailsPageState) _then;

  @override
  $Res call({
    Object link = freezed,
  }) {
    return _then(_value.copyWith(
      link: link == freezed ? _value.link : link as Link,
    ));
  }
}

abstract class $SingleLinkDetailsPageLoadingStateCopyWith<$Res>
    implements $SingleLinkDetailsPageStateCopyWith<$Res> {
  factory $SingleLinkDetailsPageLoadingStateCopyWith(
          SingleLinkDetailsPageLoadingState value,
          $Res Function(SingleLinkDetailsPageLoadingState) then) =
      _$SingleLinkDetailsPageLoadingStateCopyWithImpl<$Res>;
  @override
  $Res call({Link link});
}

class _$SingleLinkDetailsPageLoadingStateCopyWithImpl<$Res>
    extends _$SingleLinkDetailsPageStateCopyWithImpl<$Res>
    implements $SingleLinkDetailsPageLoadingStateCopyWith<$Res> {
  _$SingleLinkDetailsPageLoadingStateCopyWithImpl(
      SingleLinkDetailsPageLoadingState _value,
      $Res Function(SingleLinkDetailsPageLoadingState) _then)
      : super(_value, (v) => _then(v as SingleLinkDetailsPageLoadingState));

  @override
  SingleLinkDetailsPageLoadingState get _value =>
      super._value as SingleLinkDetailsPageLoadingState;

  @override
  $Res call({
    Object link = freezed,
  }) {
    return _then(SingleLinkDetailsPageLoadingState(
      link: link == freezed ? _value.link : link as Link,
    ));
  }
}

class _$SingleLinkDetailsPageLoadingState
    implements SingleLinkDetailsPageLoadingState {
  const _$SingleLinkDetailsPageLoadingState({@required this.link})
      : assert(link != null);

  @override
  final Link link;

  @override
  String toString() {
    return 'SingleLinkDetailsPageState.loading(link: $link)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is SingleLinkDetailsPageLoadingState &&
            (identical(other.link, link) ||
                const DeepCollectionEquality().equals(other.link, link)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(link);

  @override
  $SingleLinkDetailsPageLoadingStateCopyWith<SingleLinkDetailsPageLoadingState>
      get copyWith => _$SingleLinkDetailsPageLoadingStateCopyWithImpl<
          SingleLinkDetailsPageLoadingState>(this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result loading(Link link),
    @required Result success(Link link, List<Comment> comments),
  }) {
    assert(loading != null);
    assert(success != null);
    return loading(link);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result loading(Link link),
    Result success(Link link, List<Comment> comments),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (loading != null) {
      return loading(link);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result loading(SingleLinkDetailsPageLoadingState value),
    @required Result success(SingleLinkDetailsPageSuccessState value),
  }) {
    assert(loading != null);
    assert(success != null);
    return loading(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result loading(SingleLinkDetailsPageLoadingState value),
    Result success(SingleLinkDetailsPageSuccessState value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (loading != null) {
      return loading(this);
    }
    return orElse();
  }
}

abstract class SingleLinkDetailsPageLoadingState
    implements SingleLinkDetailsPageState {
  const factory SingleLinkDetailsPageLoadingState({@required Link link}) =
      _$SingleLinkDetailsPageLoadingState;

  @override
  Link get link;
  @override
  $SingleLinkDetailsPageLoadingStateCopyWith<SingleLinkDetailsPageLoadingState>
      get copyWith;
}

abstract class $SingleLinkDetailsPageSuccessStateCopyWith<$Res>
    implements $SingleLinkDetailsPageStateCopyWith<$Res> {
  factory $SingleLinkDetailsPageSuccessStateCopyWith(
          SingleLinkDetailsPageSuccessState value,
          $Res Function(SingleLinkDetailsPageSuccessState) then) =
      _$SingleLinkDetailsPageSuccessStateCopyWithImpl<$Res>;
  @override
  $Res call({Link link, List<Comment> comments});
}

class _$SingleLinkDetailsPageSuccessStateCopyWithImpl<$Res>
    extends _$SingleLinkDetailsPageStateCopyWithImpl<$Res>
    implements $SingleLinkDetailsPageSuccessStateCopyWith<$Res> {
  _$SingleLinkDetailsPageSuccessStateCopyWithImpl(
      SingleLinkDetailsPageSuccessState _value,
      $Res Function(SingleLinkDetailsPageSuccessState) _then)
      : super(_value, (v) => _then(v as SingleLinkDetailsPageSuccessState));

  @override
  SingleLinkDetailsPageSuccessState get _value =>
      super._value as SingleLinkDetailsPageSuccessState;

  @override
  $Res call({
    Object link = freezed,
    Object comments = freezed,
  }) {
    return _then(SingleLinkDetailsPageSuccessState(
      link: link == freezed ? _value.link : link as Link,
      comments:
          comments == freezed ? _value.comments : comments as List<Comment>,
    ));
  }
}

class _$SingleLinkDetailsPageSuccessState
    implements SingleLinkDetailsPageSuccessState {
  const _$SingleLinkDetailsPageSuccessState(
      {@required this.link, @required this.comments})
      : assert(link != null),
        assert(comments != null);

  @override
  final Link link;
  @override
  final List<Comment> comments;

  @override
  String toString() {
    return 'SingleLinkDetailsPageState.success(link: $link, comments: $comments)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is SingleLinkDetailsPageSuccessState &&
            (identical(other.link, link) ||
                const DeepCollectionEquality().equals(other.link, link)) &&
            (identical(other.comments, comments) ||
                const DeepCollectionEquality()
                    .equals(other.comments, comments)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(link) ^
      const DeepCollectionEquality().hash(comments);

  @override
  $SingleLinkDetailsPageSuccessStateCopyWith<SingleLinkDetailsPageSuccessState>
      get copyWith => _$SingleLinkDetailsPageSuccessStateCopyWithImpl<
          SingleLinkDetailsPageSuccessState>(this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result loading(Link link),
    @required Result success(Link link, List<Comment> comments),
  }) {
    assert(loading != null);
    assert(success != null);
    return success(link, comments);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result loading(Link link),
    Result success(Link link, List<Comment> comments),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (success != null) {
      return success(link, comments);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result loading(SingleLinkDetailsPageLoadingState value),
    @required Result success(SingleLinkDetailsPageSuccessState value),
  }) {
    assert(loading != null);
    assert(success != null);
    return success(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result loading(SingleLinkDetailsPageLoadingState value),
    Result success(SingleLinkDetailsPageSuccessState value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (success != null) {
      return success(this);
    }
    return orElse();
  }
}

abstract class SingleLinkDetailsPageSuccessState
    implements SingleLinkDetailsPageState {
  const factory SingleLinkDetailsPageSuccessState(
      {@required Link link,
      @required List<Comment> comments}) = _$SingleLinkDetailsPageSuccessState;

  @override
  Link get link;
  List<Comment> get comments;
  @override
  $SingleLinkDetailsPageSuccessStateCopyWith<SingleLinkDetailsPageSuccessState>
      get copyWith;
}
