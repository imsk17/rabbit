import 'dart:io';

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart' as http;
import 'package:package_info/package_info.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:rednit/rednit.dart';

import 'links.dart';
import 'secrets/reddit_constants.dart';
import 'splash/splash_page.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(RabbitApp(
    sharedPreferences: await SharedPreferences.getInstance(),
    packageInfo: await PackageInfo.fromPlatform(),
    httpClient: http.Client(),
  ));
}

class RabbitApp extends StatelessWidget {
  final SharedPreferences sharedPreferences;
  final PackageInfo packageInfo;
  final http.Client httpClient;

  const RabbitApp({
    Key key,
    @required this.sharedPreferences,
    @required this.packageInfo,
    @required this.httpClient,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final darkTheme = ThemeData.dark();
    final newDarkTheme = darkTheme.copyWith(
      textTheme: GoogleFonts.montserratTextTheme(darkTheme.textTheme).copyWith(
        body1: GoogleFonts.merriweather(textStyle: darkTheme.textTheme.body1),
        body2: GoogleFonts.merriweather(textStyle: darkTheme.textTheme.body2),
      ),
      pageTransitionsTheme: const PageTransitionsTheme(
        builders: <TargetPlatform, PageTransitionsBuilder>{
          TargetPlatform.android: ZoomPageTransitionsBuilder(),
          TargetPlatform.iOS: CupertinoPageTransitionsBuilder(),
        },
      ),
    );

    final lightTheme = ThemeData.light();
    final newLightTheme = lightTheme.copyWith(
      textTheme: GoogleFonts.montserratTextTheme(lightTheme.textTheme).copyWith(
        body1: GoogleFonts.merriweather(textStyle: lightTheme.textTheme.body1),
        body2: GoogleFonts.merriweather(textStyle: lightTheme.textTheme.body2),
      ),
      pageTransitionsTheme: const PageTransitionsTheme(
        builders: <TargetPlatform, PageTransitionsBuilder>{
          TargetPlatform.android: ZoomPageTransitionsBuilder(),
          TargetPlatform.iOS: CupertinoPageTransitionsBuilder(),
        },
      ),
    );

    return MultiProvider(
      providers: [
        ...[
          Provider.value(value: sharedPreferences),
          Provider.value(value: httpClient),
          Provider<LinksHandler>(
            create: (context) => LinksHandler(),
            dispose: (context, handler) => handler.dispose(),
          ),
        ],
        ...[
          Provider(
            create: (context) => RedditClient(
              tokenStore: SharedPreferencesTokenStore(
                sharedPreferences: Provider.of(context, listen: false),
              ),
              httpClient: Provider.of(context, listen: false),
              credentials: Credentials(
                redirectUri: RedditConstants.redirectUri,
                creatorUsername: RedditConstants.creatorUsername,
                clientId: RedditConstants.clientId,
                appVersion: packageInfo.version,
                packageId: packageInfo.packageName,
                platformName: Platform.operatingSystem,
              ),
            ),
            lazy: false,
          ),
        ],
      ],
      child: MaterialApp(
        title: 'Rabbit',
        themeMode: ThemeMode.system,
        theme: newLightTheme,
        darkTheme: newDarkTheme,
        home: SplashPage(),
      ),
    );
  }
}
