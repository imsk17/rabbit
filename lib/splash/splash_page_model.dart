import 'package:flutter/foundation.dart';
import 'package:simple_model_state/simple_model_state.dart';
import 'package:rednit/rednit.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SplashPageModel extends BaseModel {
  final SharedPreferences _sharedPreferences;
  final RedditClient _redditClient;
  final Function() _onDone;

  SplashPageModel({
    @required SharedPreferences sharedPreferences,
    @required RedditClient redditClient,
    @required Function() onDone,
  })  : _redditClient = redditClient,
        _sharedPreferences = sharedPreferences,
        _onDone = onDone {
    _init();
  }

  Future<void> _init() async {
    await Future<void>.delayed(const Duration(seconds: 1));
    await _onDone();
  }
}
