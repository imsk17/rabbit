import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:rednit/rednit.dart';

part 'subreddit_page_state.freezed.dart';

@freezed
abstract class SubredditPageState with _$SubredditPageState {
  const factory SubredditPageState.loading() = _SubredditPageStateLoading;
  const factory SubredditPageState.success({
    @required Subreddit subreddit,
    @required List<Link> links,
  }) = _SubredditPageStateSuccess;
}
