import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:characters/characters.dart';
import 'package:gradient_text/gradient_text.dart';
import 'package:rednit/rednit.dart';
import 'package:better_color/better_color.dart';

import 'subreddit_icon.dart';
import 'vote_widget.dart';
import '../utilities/date_format.dart';
import '../utilities/list_sum.dart';
import '../subreddit/subreddit_page.dart';

class LinkCard extends StatelessWidget {
  final Link link;
  final void Function() onTap;
  final bool expanded;

  LinkCard({
    Key key,
    @required this.link,
    this.onTap,
    this.expanded = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final hueValue = link.subreddit.codeUnits.sum() % 360.0;
    final baseColor = Colors.white;
    final subredditColor = baseColor.withSaturation(0.63).withHue(hueValue);
    final subredditGradientColor = subredditColor.hueSpin(30);

    return Column(
      children: <Widget>[
        Material(
          // color: color,
          child: InkWell(
            onTap: onTap,
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(bottom: 8.0),
                    child: IntrinsicHeight(
                      child: Row(
                        children: <Widget>[
                          SubredditIcon(subredditName: link.subreddit),
                          SizedBox(width: 8.0),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              GestureDetector(
                                onTap: () => Navigator.of(context).push(
                                  MaterialPageRoute<void>(
                                    builder: (context) => SubredditPage(
                                      subredditDisplayName: link.subreddit,
                                    ),
                                  ),
                                ),
                                child: Text(
                                  'r/${link.subreddit}',
                                  overflow: TextOverflow.ellipsis,
                                  // style: TextStyle(fontSize: 18),
                                ),
                              ),
                              Text(
                                'Posted by u/${link.author}'
                                ' • ${DateTime.now().difference(link.created).readableShort}',
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(
                                  color: Theme.of(context)
                                      .textTheme
                                      .body1
                                      .color
                                      .withOpacity(0.54),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 8.0),
                    child: Text(
                      link.title,
                      style: Theme.of(context)
                          .textTheme
                          .title
                          .copyWith(fontSize: 18.0),
                      maxLines: 4,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 8.0),
                    child: (() {
                      if (link.postHint == null) {
                        if (link.selftext == null || !expanded) {
                          return Container();
                        } else {
                          return Text(link.selftext);
                        }
                      }

                      switch (link.postHint) {
                        case 'image':
                          return _ImageTypeWidget(
                            thumbnailUrl: link.previewThumbnailImages.first.url,
                            imageUrl: link.previewThumbnailImages.last.url,
                            expanded: expanded,
                          );
                        case 'link':
                          return _LinkTypeWidget(
                            linkUrl: link.url,
                            thumbnailUrl: link.thumbnail,
                          );
                        case 'self':
                          if (expanded) {
                            return Text(link.selftext);
                          }
                          return Container();
                        default:
                          return Center(
                            child: Text(
                              'Implement type ${link.postHint}',
                              style: Theme.of(context)
                                  .textTheme
                                  .display1
                                  .copyWith(fontWeight: FontWeight.bold),
                            ),
                          );
                      }
                    })(),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Expanded(
                        child: VoteWidget(
                          score: link.score,
                        ),
                      ),
                      Expanded(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              '${link.numComments}',
                              style: Theme.of(context).textTheme.button,
                            ),
                            SizedBox(width: 8.0),
                            FaIcon(
                              FontAwesomeIcons.commentAlt,
                              size: 16.0,
                            ),
                            // Icon(Icons.mode_comment),
                          ],
                        ),
                      ),
                      Expanded(child: Row(children: <Widget>[])),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
        // Divider(),
        Container(height: 8),
      ],
    );
  }
}

class _ImageTypeWidget extends StatelessWidget {
  const _ImageTypeWidget({
    Key key,
    @required this.thumbnailUrl,
    @required this.imageUrl,
    @required this.expanded,
  }) : super(key: key);

  final String imageUrl, thumbnailUrl;
  final bool expanded;

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(8.0),
      child: ConstrainedBox(
        constraints: expanded
            ? BoxConstraints.tightFor()
            : BoxConstraints(maxHeight: 200),
        child: Row(
          children: <Widget>[
            Expanded(
              child: Image.network(
                // expanded ? imageUrl : thumbnailUrl,
                imageUrl,
                fit: BoxFit.cover,
                alignment: Alignment.center,
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class _LinkTypeWidget extends StatelessWidget {
  final String thumbnailUrl;
  final Uri linkUrl;

  const _LinkTypeWidget({
    Key key,
    @required this.thumbnailUrl,
    @required this.linkUrl,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(8.0),
      child: IntrinsicHeight(
        child: Row(
          children: <Widget>[
            Expanded(
              child: Container(
                color: Colors.black12,
              ),
            ),
            Stack(
              alignment: Alignment.bottomCenter,
              children: <Widget>[
                Positioned.fill(
                  child: Image.network(
                    thumbnailUrl,
                    fit: BoxFit.cover,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 48.0),
                  child: Container(
                    color: Colors.black87,
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      linkUrl.authority,
                      style: TextStyle(
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
