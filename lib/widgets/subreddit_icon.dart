import 'package:flutter/material.dart';
import 'package:better_color/better_color.dart';
import 'package:gradient_text/gradient_text.dart';
import 'package:characters/characters.dart';

import '../utilities/list_sum.dart';

class SubredditIcon extends StatelessWidget {
  final String subredditName;

  const SubredditIcon({
    Key key,
    @required this.subredditName,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final hueValue = subredditName.codeUnits.sum() % 360.0;
    final baseColor = Colors.white;
    final subredditColor = baseColor.withSaturation(0.63).withHue(hueValue);
    final subredditGradientColor = subredditColor.hueSpin(30);

    return AspectRatio(
      aspectRatio: 1,
      child: FittedBox(
        fit: BoxFit.contain,
        child: GradientText(
          subredditName.characters.first.toUpperCase(),
          gradient: LinearGradient(colors: [
            subredditColor,
            subredditGradientColor,
          ]),
          style: Theme.of(context)
              .textTheme
              .headline
              .copyWith(fontWeight: FontWeight.bold),
        ),
      ),
    );
  }
}
